# Table of Contents
[[_TOC_]]

# About
A small library for bayesian filtering: Filter Library IRT (Institut für Regelungstechink / Institute of Automatic Control, RWTH Aachen University).
It does not try to be an "Eierlegende Wollmilchsau" as it accepts that the filter models are just too different.
So instead of dependency injection nightmares or template metaprogramming hell a few simple filters are provided, each with its own data representation.

# Setup
We use the [wstool](http://wiki.ros.org/wstool) to setup our workspace.
Install it via:
```bash
# install wstool
sudo apt install python-wstool
```
Now we can setup the a catkin workspace via the wstool.
You might want to change the URL whether you use SSH or HTTPS to clone repositories.
```bash
# init workspace
mkdir -p my_catkin_ws && cd my_catkin_ws
wstool init src
# add flirt
wstool set -y -t src --git flirt https://gitlab.com/rwth-irt-public/flirt/flirt.git
wstool update -t src
```

After setting up the workspace, the system dependencies can be installed via rosdep:
```bash
rosdep install --from-paths src --ignore-src -r -y
```
# Build the Catkin Workspace
As `catkin_make` does not support mixed CMake and catkin workspaces, you must use either `catkin_make_isolated` oder `catkin build` to compile the workspace.
We recommend the latter for faster compile times on multi-core system.
It is part of the [catkin_tools](https://catkin-tools.readthedocs.io/en/latest/index.html).

# Unit Tests
Under Ubuntu GTest only installs the sources.
To build GTest run:
```sh
sudo apt install libgtest-dev build-essential cmake
cd /usr/src/googletest
sudo cmake .
sudo cmake --build . --target install
```

You can use the following commands to run the tests
```bash
# release optimizations cause tests to fail
catkin config --cmake-args -DCMAKE_BUILD_TYPE=Debug
# rebuild
catkin build flirt
# run tests manually
catkin test --no-deps flirt
```

# Usage
The classes are all templated. While the motivation for the Kalman filter is to
verify the model dimensions at compile time, the particle filter can actually
use generic transition and observation models.

The typical workflow is to define the generic filter and then retrieving the
concrete model types from it. 
See the [test classes](test) on how to create a filter instance.

A rather extensive example / use case is given in our [6D object pose tracker](https://gitlab.com/rwth-irt-public/flirt/pose_tracker).
There, you will find custom transition and likelihood functions as well as parallelization on th CPU and GPU.

# Dependencies 
Installable via rosdep:
- [Eigen3](http://eigen.tuxfamily.org/index.php?title=Main_Page): Linear Algebra
- [Google Test](https://github.com/google/googletest): Unit Tests

# Continous Integration
A gitlab CI pipeline is implemented, which is based on [ros_gitlab_ci](https://gitlab.com/VictorLamoine/ros_gitlab_ci). 
The CI is based on the `ros:melodic-ros-core` docker image and will install the dependencies via rosdep. The gitlab-runner must use a **docker** executor.

# Citing
If you use parts of this library in your scientific publication, please consider citing:
```
@article { 3Dcamerabasedmarkerlessnavigationsystemforroboticosteotomies,
      author = "Tim Übelhör and Jonas Gesenhues and Nassim Ayoub and Ali Modabber and Dirk Abel",
      title = "3D camera-based markerless navigation system for robotic osteotomies",
      journal = "at - Automatisierungstechnik",
      year = "01 Oct. 2020",
      publisher = "De Gruyter",
      address = "Berlin, Boston",
      volume = "68",
      number = "10",
      doi = "https://doi.org/10.1515/auto-2020-0032",
      pages=      "863 - 879",
      url = "https://www.degruyter.com/view/journals/auto/68/10/article-p863.xml"
}
```

# Funding
Funded by the Excellence Initiative of the German federal and state governments.
