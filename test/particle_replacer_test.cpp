/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <gtest/gtest.h>
#include <flirt/utility/particle_replacer.hpp>
using namespace flirt;

const int SEED = 42;
ParticleReplacer replacer(SEED);
const size_t N_SAMPLES = 50000;
const std::vector<int> OLD_STATES(N_SAMPLES, 0);
const std::vector<int> NEW_STATES(N_SAMPLES, 1);
const double ABS_ERROR = 0.1;

void test_ratio(const std::vector<int>& states, double ratio)
{
  double mean = 0;
  for (const auto& state : states)
  {
    mean += (double)state / N_SAMPLES;
  }
  ASSERT_NEAR(mean, ratio, ABS_ERROR);
}

void test_ratio(double ratio)
{
  double mean = 0;
  for (size_t i = 0; i < N_SAMPLES; i++)
  {
    mean += (double)replacer.select(0, 1, ratio) / N_SAMPLES;
  }
  ASSERT_NEAR(mean, ratio, ABS_ERROR);
}

TEST(ParticleReplacerTest, TestReplaceVector)
{
  // test different ratios
  double ratio = 0;
  auto replaced_states = replacer.replace(OLD_STATES, NEW_STATES, ratio);
  test_ratio(replaced_states, ratio);
  ratio = 0.1;
  replaced_states = replacer.replace(OLD_STATES, NEW_STATES, ratio);
  test_ratio(replaced_states, ratio);
  ratio = 0.3;
  replaced_states = replacer.replace(OLD_STATES, NEW_STATES, ratio);
  test_ratio(replaced_states, ratio);
  ratio = 0.5;
  replaced_states = replacer.replace(OLD_STATES, NEW_STATES, ratio);
  test_ratio(replaced_states, ratio);
  ratio = 0.9;
  replaced_states = replacer.replace(OLD_STATES, NEW_STATES, ratio);
  test_ratio(replaced_states, ratio);
  ratio = 1;
  replaced_states = replacer.replace(OLD_STATES, NEW_STATES, ratio);
  test_ratio(replaced_states, ratio);
}

TEST(ParticleReplacerTest, TestReplaceState)
{
  // test different ratios
  double ratio = 0;
  auto replaced_states = replacer.replace(OLD_STATES, 1, ratio);
  test_ratio(replaced_states, ratio);
  ratio = 0.1;
  replaced_states = replacer.replace(OLD_STATES, 1, ratio);
  test_ratio(replaced_states, ratio);
  ratio = 0.3;
  replaced_states = replacer.replace(OLD_STATES, 1, ratio);
  test_ratio(replaced_states, ratio);
  ratio = 0.5;
  replaced_states = replacer.replace(OLD_STATES, 1, ratio);
  test_ratio(replaced_states, ratio);
  ratio = 0.9;
  replaced_states = replacer.replace(OLD_STATES, 1, ratio);
  test_ratio(replaced_states, ratio);
  ratio = 1;
  replaced_states = replacer.replace(OLD_STATES, 1, ratio);
  test_ratio(replaced_states, ratio);
}

TEST(ParticleReplacerTest, TestSelect)
{
  // test different ratios
  double ratio = 0;
  test_ratio(ratio);
  ratio = 0.1;
  test_ratio(ratio);
  ratio = 0.3;
  test_ratio(ratio);
  ratio = 0.5;
  test_ratio(ratio);
  ratio = 0.9;
  test_ratio(ratio);
  ratio = 1;
  test_ratio(ratio);
}
