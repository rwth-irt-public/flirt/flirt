/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <gtest/gtest.h>
#include <flirt/utility/uniform_sampler.hpp>
using namespace flirt;

const int SEED = 42;
const size_t N_SAMPLES = 50000;
const double LOWER = -12;
const double UPPER = 42;
const double MEAN = LOWER + (UPPER - LOWER) / 2;
const double ABS_ERROR = 0.1;

TEST(UniformSamplerTest, TestBounded)
{
  UniformSampler sampler(SEED);
  std::array<double, N_SAMPLES> random_numbers;
  // test generate
  for (auto& value : random_numbers)
  {
    value = sampler.generate(LOWER, UPPER);
    // All in interval?
    ASSERT_GE(value, LOWER);
    ASSERT_LT(value, UPPER);
  }
  // mean okay?
  double mean_gen = 0;
  for (auto value : random_numbers)
  {
    mean_gen += value / N_SAMPLES;
  }
  ASSERT_NEAR(mean_gen, MEAN, ABS_ERROR);
  // test operator
  for (auto& value : random_numbers)
  {
    value = sampler(LOWER, UPPER);
    // All in interval?
    ASSERT_GE(value, LOWER);
    ASSERT_LT(value, UPPER);
  }
  // mean okay?
  double mean_op = 0;
  for (auto value : random_numbers)
  {
    mean_op += value / N_SAMPLES;
  }
  ASSERT_NEAR(mean_op, MEAN, ABS_ERROR);
}
