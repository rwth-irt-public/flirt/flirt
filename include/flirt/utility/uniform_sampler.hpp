/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <random>

namespace flirt
{
/*!
Simplifies drawing from a uniform distribution by supplying a true random seed.
Uses mt19937 random number generator with this seed.
*/
class UniformSampler
{
public:
  UniformSampler()
  {
    std::random_device device;
    generator.seed(device());
  }

  /**
   * Create a uniform random sample with a custom seed (for testing)
   */
  UniformSampler(unsigned int seed) : generator(seed)
  {
  }

  /**
   * Create a uniform random sample with a custom generator (for testing)
   */
  UniformSampler(std::mt19937 gen) : generator(gen)
  {
  }

  /*!
  Generates a float random value x with lower<=x<upper
  */
  double generate(double lower, double upper)
  {
    std::uniform_real_distribution<double> dist(lower, upper);
    return dist(generator);
  }

  /*!
  Generates a float random value x with lower<=x<upper
  */
  double operator()(double lower, double upper)
  {
    return generate(lower, upper);
  }

private:
  std::mt19937 generator;
};
}  // namespace flirt