/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <algorithm>
#include <cmath>
#include <flirt/utility/uniform_sampler.hpp>
#include <functional>
#include <vector>

namespace flirt
{
/**
 * A particle filter which uses systematic resampling.
 */
template <typename StateType>
class ParticleFilter
{
public:
  /**
   * Create a logarithmic particle with the given number of particles.
   * @param particle_count the number of particles to initialize
   */
  ParticleFilter(size_t particle_count)
    : weights(particle_count), states(particle_count), particle_count(particle_count)
  {
  }

  /**
   * Set the initial state belief. Must have the size particle_count.
   * Distributes the weights uniformly
   * @param inital_states possible initial states
   */
  void initialize(std::vector<StateType> initial_states)
  {
    if (initial_states.size() == particle_count)
    {
      states = std::move(initial_states);
      double avg_weight = 1.0 / particle_count;
      for (double& current : weights)
      {
        current = avg_weight;
      }
    }
  }

  /**
   * Set the priors of the filter step.
   * @param predictions prior states
   */
  void set_predictions(std::vector<StateType> predictions)
  {
    states = std::move(predictions);
  }

  /**
   * Updates the particle weights by incorporating the observation as a batch.
   * The update step is done in a SIR bootstrap filter fashion. Resampling is
   * performed with the low variance resampling method.
   * @param likelihoods the logarithmic likelihoods for all the states (in
   * the same order as these states)
   */
  void update(const std::vector<double>& likelihoods)
  {
    double weight_sum = 0;
    double max_weight = -std::numeric_limits<double>::infinity();
    max_like = -std::numeric_limits<double>::infinity();
    // weights as posterior of observation
    for (size_t i = 0; i < particle_count; i++)
    {
      // Using the prior as proposal so the weight recursion is simply:
      weights[i] *= likelihoods[i];
      if (likelihoods[i] > max_like)
      {
        max_like = likelihoods[i];
      }
      // check for MAP here, after resampling the weights are all equal
      if (weights[i] > max_weight)
      {
        map_state = states[i];
        max_weight = weights[i];
      }
      weight_sum += weights[i];
    }
    // Normalize weights
    double normalize_const = 1 / weight_sum;
    for (auto& current : weights)
    {
      current *= normalize_const;
    }
    // resample if effective sample size is smaller than the half of particle
    // count
    n_eff = effective_sample_size(weights);
    if (n_eff < particle_count / 2.0)
    {
      resample();
    }
  }

  /**
   * Returns the maximum-a-posteriori state from the last update step.
   */
  StateType get_map_state() const
  {
    return map_state;
  }

  /**
   * Returns the effective sample size.
   */
  static double effective_sample_size(const std::vector<double>& weight_vec)
  {
    double sum_of_squares = 0;
    for (double value : weight_vec)
    {
      sum_of_squares += value * value;
    }
    return 1.0 / sum_of_squares;
  }

  /**
   * Returns maximum log-likelihood from the last update step
   */
  double max_likelihood() const
  {
    return max_like;
  }

  /**
   * Returns the state of the particles. Use get_log_weights to obtain the full
   * belief.
   */
  const std::vector<StateType>& get_states() const
  {
    return states;
  }

  /**
   * Returns the weights of the particles. Use get_states to obtain the full
   * belief.
   */
  const std::vector<double>& get_weights() const
  {
    return weights;
  }

  /**
   * Returns the number of particles beeing simulated
   */
  size_t get_particle_count() const
  {
    return particle_count;
  }

  /**
   * Systematic resampling of the particles.
   */
  void resample()
  {
    // Make copy of old belief
    std::vector<StateType> old_states = states;
    std::vector<double> old_weights = weights;
    // Start at random value within average weight
    double cumulative = old_weights[0];
    double avg_weight = 1.0 / particle_count;
    double start_weight = uniform_sampler(0, avg_weight);
    // indices: o in old, n in new
    size_t o = 0;
    for (size_t n = 0; n < particle_count; n++)
    {
      double U = start_weight + n * avg_weight;
      while (U > cumulative)
      {
        o++;
        cumulative += old_weights[o];
      }
      // Resample this particle and reset the weight
      states[n] = old_states[o];
      weights[n] = avg_weight;
    }
  }

  /**
   * Set the number of simulated particles. The particles are resized and it
   * is advised to call initialize after setting the particle count.
   */
  void set_particle_count(size_t count)
  {
    particle_count = count;
    states.resize(particle_count);
    weights.resize(particle_count);
  }

private:
  // corrsponding states and weights
  std::vector<double> weights;
  std::vector<StateType> states;
  // parameters
  size_t particle_count;
  // maximum-a-posteriori state;
  StateType map_state;
  double n_eff;
  double max_like;
  // sample from uniform distribution
  UniformSampler uniform_sampler;
};
}  // namespace flirt